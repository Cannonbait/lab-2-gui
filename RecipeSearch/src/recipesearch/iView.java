/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package recipesearch;

import java.beans.PropertyChangeListener;
import java.util.List;
import se.chalmers.ait.dat215.lab2.Recipe;

/**
 *
 * @author Ivar
 */
public interface iView {
    public void addListener(PropertyChangeListener listener);
    public void displaySearchRecipes(List<Recipe> recipes);
    public void displaySpecificRecipe(Recipe recipe);
}
