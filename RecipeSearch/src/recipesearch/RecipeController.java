/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recipesearch;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import se.chalmers.ait.dat215.lab2.Recipe;
import se.chalmers.ait.dat215.lab2.RecipeDatabase;
import se.chalmers.ait.dat215.lab2.SearchFilter;

/**
 *
 * @author Ivar
 */
public class RecipeController implements PropertyChangeListener{
    private iView view;
    private RecipeDatabase db;
    public RecipeController(iView view) {
        db = RecipeDatabase.getSharedInstance();
        view.addListener(this);
        this.view = view;

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("search")) {
            view.displaySearchRecipes(db.search((SearchFilter)evt.getNewValue()));
        }
        else if (evt.getPropertyName().equals("choose")) {
            view.displaySpecificRecipe(((RecipeShort) evt.getNewValue()).getRecipe());
        }
    }

}
