package recipesearch;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import javax.swing.*;
import se.chalmers.ait.dat215.lab2.Recipe;
import se.chalmers.ait.dat215.lab2.SearchFilter;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class RecipeSearchView extends javax.swing.JFrame implements iView, MouseListener {

    /**
     * Creates new form ExampleApplicationView
     */
    public RecipeSearchView() {
        initComponents();
        setVisible(true);
        for (int i = 0; i < 5; i++) {
            results[i] = new RecipeShort();
            results[i].setVisible(false);
            results[i].addMouseListener(this);
            resultsHolder.add(results[i], i);
        }
        pack();
    }

    
    /**
     * Add a listener to the PropertyChangeSupport eventfiring list
     * @param listener the class to be added
     */
    @Override
    public void addListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    /**
     * Method called by the controller to display a list of search results
     * @param recipes is a sorted list that contains recipes after match
     */
    @Override
    public void displaySearchRecipes(List<Recipe> recipes) {
        for (int i = 0; i < 5; i++) {
            results[i].setRecipe(recipes.get(i));
            results[i].setVisible(true);
        }
        revalidate();
    }

    /**
     * Method called by controller when we want the view to display one specific recipe
     * @param recipe the recipe to be displayed
     */
    @Override
    public void displaySpecificRecipe(Recipe recipe) {
        tabSpecific.setTitleAt(1, recipe.getName());
        recipeName.setText(recipe.getName());
        recipeMinutes.setText("" + recipe.getTime());
        recipeServings.setText("" + recipe.getServings());
        recipeIngredients.setText(Utils.listToString(recipe.getIngredients()));
        recipeInstructions.setText(recipe.getDescription());
        tabSpecific.setSelectedIndex(1);
    }
    
    /**
     * Gives the users input on what cuisine they want to search for.
     * @return null if no user input, otherwise the kitchen
     */
    private String getCuisine() {
        String cuisine = kitchen.getSelectedItem().toString();
        if (cuisine.equals(" ")) {
            return null;
        }
        return cuisine;
    }

    /**
     * Gives the users input on the maximum price
     * @return user input if it can be parsed to an int, otherwise 0
     */
    private int getMaxPrice() {
        try {
            return Math.abs(Integer.parseInt(inputMaxpris.getText()));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Returns user input for main ingredient
     * @return null if no user input, otherwise one of "Kött", "Fisk", "Kyckling", "Vegetarisk"
     */
    private String getMainIngredient() {
        ButtonModel radio = mainIngredient.getSelection();
        switch (radio.getActionCommand()) {
            case "1":
                return ("Kött");
            case "2":
                return ("Fisk");
            case "3":
                return ("Kyckling");
            case "4":
                return ("Vegetarisk");
            default:
                return (null);
        }
    }

    /**
     * Returns the user input for wanted difficulty.
     * @return One of null, "Lätt", "Medel", "Svår"
     */
    private String getDifficulty() {
        ButtonModel radio = difficultyLevel.getSelection();
        switch (radio.getActionCommand()) {
            case "1":
                return ("Lätt");
            case "2":
                return ("Medel");
            case "3":
                return ("Svår");
            default:
                return (null);
        }
    }

    /**
     * Gets the user input for max time
     * @return The current value of he slider
     */
    private int getMaxTime() {
        return maxtimeSlider.getValue();
    }
    /**
     * Creates a new searchflter based of he values of he paapeters
     * @return A new search filter
     */
    private SearchFilter createFilter() {
        return new SearchFilter(getDifficulty(), getMaxTime(), getCuisine(), getMaxPrice(), getMainIngredient());
    }
    
    @Override
    
    public void mouseClicked(MouseEvent e) {
        
    }
    /**
     * Fires a propetrychange object to the liseners when a the mousebutton is pressed
     * @param e The event created from he mousepress
     */
    @Override
    public void mousePressed(MouseEvent e) {
        listeners.firePropertyChange("choose", null, e.getSource());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        mainIngredient = new javax.swing.ButtonGroup();
        difficultyLevel = new javax.swing.ButtonGroup();
        tabSpecific = new javax.swing.JTabbedPane();
        splitSearchResult = new javax.swing.JSplitPane();
        searchArea = new javax.swing.JPanel();
        searchKitchen = new javax.swing.JLabel();
        searchMain = new javax.swing.JLabel();
        searchDifficulty = new javax.swing.JLabel();
        searchMaxtime = new javax.swing.JLabel();
        maxprice = new javax.swing.JLabel();
        kitchen = new javax.swing.JComboBox();
        inputMaxpris = new javax.swing.JTextField();
        meat = new javax.swing.JRadioButton();
        fish = new javax.swing.JRadioButton();
        chicken = new javax.swing.JRadioButton();
        vegetarian = new javax.swing.JRadioButton();
        everything = new javax.swing.JRadioButton();
        easy = new javax.swing.JRadioButton();
        intermediate = new javax.swing.JRadioButton();
        hard = new javax.swing.JRadioButton();
        maxtimeSlider = new javax.swing.JSlider();
        search = new javax.swing.JButton();
        maxtimeDisplay = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        difficultyFree = new javax.swing.JRadioButton();
        resultScroll = new javax.swing.JScrollPane();
        resultArea = new javax.swing.JPanel();
        header = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        resultsHolder = new javax.swing.JPanel();
        tabSpecificResult = new javax.swing.JSplitPane();
        jSplitPane3 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        recipeServings = new javax.swing.JLabel();
        recipeDescServings = new javax.swing.JLabel();
        recipeMinutes = new javax.swing.JLabel();
        recipeDescMinutes = new javax.swing.JLabel();
        recipeDescIngredients = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        recipeIngredients = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        recipeDescInstructions = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        recipeInstructions = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        recipeName = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("recipesearch/resources/RecipeSearch"); // NOI18N
        setTitle(bundle.getString("Application.title")); // NOI18N
        setMinimumSize(new java.awt.Dimension(1100, 700));
        setName("applicationFrame"); // NOI18N

        splitSearchResult.setDividerLocation(225);

        searchKitchen.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        searchKitchen.setText("Kök");

        searchMain.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        searchMain.setText("Huvudingrediens");

        searchDifficulty.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        searchDifficulty.setText("Svårighetsgrad");

        searchMaxtime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        searchMaxtime.setText("Maxtid:");

        maxprice.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        maxprice.setText("Maxpris");

        kitchen.setModel(new javax.swing.DefaultComboBoxModel(kitchens));

        mainIngredient.add(meat);
        meat.setText("Kött");
        meat.setActionCommand("1");

        mainIngredient.add(fish);
        fish.setText("Fisk");
        fish.setActionCommand("2");

        mainIngredient.add(chicken);
        chicken.setText("Kyckling");
        chicken.setActionCommand("3");

        mainIngredient.add(vegetarian);
        vegetarian.setText("Vegetarisk");
        vegetarian.setActionCommand("4");

        mainIngredient.add(everything);
        everything.setSelected(true);
        everything.setText("Godtycklig");
        everything.setActionCommand("0");

        difficultyLevel.add(easy);
        easy.setText("Lätt");
        easy.setActionCommand("1");

        difficultyLevel.add(intermediate);
        intermediate.setText("Medel");
        intermediate.setActionCommand("2"); // NOI18N

        difficultyLevel.add(hard);
        hard.setText("Svår");
        hard.setActionCommand("3");

        maxtimeSlider.setMajorTickSpacing(50);
        maxtimeSlider.setMaximum(150);
        maxtimeSlider.setMinorTickSpacing(10);
        maxtimeSlider.setPaintLabels(true);
        maxtimeSlider.setPaintTicks(true);
        maxtimeSlider.setSnapToTicks(true);
        maxtimeSlider.setValue(0);
        maxtimeSlider.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                maxtimeSliderPropertyChange(evt);
            }
        });

        search.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        search.setText("Sök");
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, maxtimeSlider, org.jdesktop.beansbinding.ELProperty.create("${value}"), maxtimeDisplay, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Sök");

        difficultyLevel.add(difficultyFree);
        difficultyFree.setSelected(true);
        difficultyFree.setText("Godtycklig");
        difficultyFree.setActionCommand("0");

        javax.swing.GroupLayout searchAreaLayout = new javax.swing.GroupLayout(searchArea);
        searchArea.setLayout(searchAreaLayout);
        searchAreaLayout.setHorizontalGroup(
            searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchAreaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(searchAreaLayout.createSequentialGroup()
                        .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(searchAreaLayout.createSequentialGroup()
                                .addComponent(searchKitchen)
                                .addGap(44, 44, 44)
                                .addComponent(maxprice))
                            .addComponent(meat)
                            .addComponent(searchMain)
                            .addComponent(fish)
                            .addComponent(chicken)
                            .addComponent(vegetarian)
                            .addComponent(everything)
                            .addGroup(searchAreaLayout.createSequentialGroup()
                                .addComponent(kitchen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(inputMaxpris, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(searchAreaLayout.createSequentialGroup()
                        .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(difficultyFree)
                            .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(hard)
                                .addComponent(intermediate)
                                .addComponent(easy)
                                .addComponent(searchDifficulty)
                                .addGroup(searchAreaLayout.createSequentialGroup()
                                    .addComponent(searchMaxtime)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(maxtimeDisplay))
                                .addComponent(maxtimeSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        searchAreaLayout.setVerticalGroup(
            searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchAreaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchKitchen)
                    .addComponent(maxprice))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kitchen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputMaxpris, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchMain)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(meat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fish)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chicken)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(vegetarian)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(everything)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchDifficulty)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(easy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(intermediate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hard)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(difficultyFree)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(searchAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchMaxtime)
                    .addComponent(maxtimeDisplay))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(maxtimeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(search, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addGap(29, 29, 29))
        );

        splitSearchResult.setLeftComponent(searchArea);

        resultScroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        resultScroll.setMaximumSize(new java.awt.Dimension(850, 1500));
        resultScroll.setPreferredSize(new java.awt.Dimension(850, 117));

        resultArea.setLayout(new java.awt.BorderLayout());

        header.setMaximumSize(new java.awt.Dimension(1000, 32767));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Resultat");

        javax.swing.GroupLayout headerLayout = new javax.swing.GroupLayout(header);
        header.setLayout(headerLayout);
        headerLayout.setHorizontalGroup(
            headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headerLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 594, Short.MAX_VALUE))
                    .addComponent(jSeparator1))
                .addContainerGap())
        );
        headerLayout.setVerticalGroup(
            headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        resultArea.add(header, java.awt.BorderLayout.PAGE_START);

        resultsHolder.setLayout(new java.awt.GridLayout(5, 0, 0, 10));
        resultArea.add(resultsHolder, java.awt.BorderLayout.CENTER);

        resultScroll.setViewportView(resultArea);

        splitSearchResult.setRightComponent(resultScroll);

        tabSpecific.addTab("Sök", splitSearchResult);

        tabSpecificResult.setDividerLocation(75);
        tabSpecificResult.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jSplitPane3.setDividerLocation(225);

        recipeServings.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        recipeServings.setText("X");

        recipeDescServings.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        recipeDescServings.setText("Portioner");

        recipeMinutes.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        recipeMinutes.setText("XXX");

        recipeDescMinutes.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        recipeDescMinutes.setText("Minuter");

        recipeDescIngredients.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        recipeDescIngredients.setText("Ingredienser");

        recipeIngredients.setEditable(false);
        recipeIngredients.setColumns(20);
        recipeIngredients.setLineWrap(true);
        recipeIngredients.setRows(5);
        recipeIngredients.setWrapStyleWord(true);
        jScrollPane1.setViewportView(recipeIngredients);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(recipeServings)
                                    .addComponent(recipeMinutes))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(recipeDescServings)
                                    .addComponent(recipeDescMinutes)))
                            .addComponent(recipeDescIngredients))
                        .addGap(0, 107, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(recipeMinutes)
                    .addComponent(recipeDescMinutes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(recipeServings)
                    .addComponent(recipeDescServings))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(recipeDescIngredients)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(135, Short.MAX_VALUE))
        );

        jSplitPane3.setLeftComponent(jPanel3);

        recipeDescInstructions.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        recipeDescInstructions.setText("Instruktioner");

        recipeInstructions.setEditable(false);
        recipeInstructions.setColumns(20);
        recipeInstructions.setLineWrap(true);
        recipeInstructions.setRows(5);
        recipeInstructions.setWrapStyleWord(true);
        jScrollPane2.setViewportView(recipeInstructions);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(recipeDescInstructions)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(recipeDescInstructions)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane3.setRightComponent(jPanel5);

        tabSpecificResult.setBottomComponent(jSplitPane3);

        recipeName.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        recipeName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        recipeName.setText("NAMN");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(recipeName, javax.swing.GroupLayout.DEFAULT_SIZE, 977, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(recipeName, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabSpecificResult.setLeftComponent(jPanel4);

        tabSpecific.addTab("NAMN", tabSpecificResult);

        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("recipesearch/resources/RecipeSearchView"); // NOI18N
        fileMenu.setText(bundle1.getString("fileMenu.text")); // NOI18N

        exitMenuItem.setText(bundle1.getString("exitApplicationMenuItem.text")); // NOI18N
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        jMenuBar1.add(fileMenu);

        helpMenu.setText(bundle1.getString("helpMenu.text")); // NOI18N

        aboutMenuItem.setText(bundle1.getString("aboutMenuItem.text")); // NOI18N
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabSpecific, javax.swing.GroupLayout.DEFAULT_SIZE, 1008, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabSpecific, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        new RecipeSearchAboutBox(this).setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void maxtimeSliderPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_maxtimeSliderPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_maxtimeSliderPropertyChange

    private void searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchActionPerformed
        listeners.firePropertyChange("search", null, createFilter());
    }//GEN-LAST:event_searchActionPerformed

    private final RecipeShort[] results = new RecipeShort[5];
    private final String[] kitchens = {" ", "Sverige", "Grekland", "Indien", "Asien", "Afrika", "Frankrike"};
    private final PropertyChangeSupport listeners = new PropertyChangeSupport(this);
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JRadioButton chicken;
    private javax.swing.JRadioButton difficultyFree;
    private javax.swing.ButtonGroup difficultyLevel;
    private javax.swing.JRadioButton easy;
    private javax.swing.JRadioButton everything;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JRadioButton fish;
    private javax.swing.JRadioButton hard;
    private javax.swing.JPanel header;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JTextField inputMaxpris;
    private javax.swing.JRadioButton intermediate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JComboBox kitchen;
    private javax.swing.ButtonGroup mainIngredient;
    private javax.swing.JLabel maxprice;
    private javax.swing.JLabel maxtimeDisplay;
    private javax.swing.JSlider maxtimeSlider;
    private javax.swing.JRadioButton meat;
    private javax.swing.JLabel recipeDescIngredients;
    private javax.swing.JLabel recipeDescInstructions;
    private javax.swing.JLabel recipeDescMinutes;
    private javax.swing.JLabel recipeDescServings;
    private javax.swing.JTextArea recipeIngredients;
    private javax.swing.JTextArea recipeInstructions;
    private javax.swing.JLabel recipeMinutes;
    private javax.swing.JLabel recipeName;
    private javax.swing.JLabel recipeServings;
    private javax.swing.JPanel resultArea;
    private javax.swing.JScrollPane resultScroll;
    private javax.swing.JPanel resultsHolder;
    private javax.swing.JButton search;
    private javax.swing.JPanel searchArea;
    private javax.swing.JLabel searchDifficulty;
    private javax.swing.JLabel searchKitchen;
    private javax.swing.JLabel searchMain;
    private javax.swing.JLabel searchMaxtime;
    private javax.swing.JSplitPane splitSearchResult;
    private javax.swing.JTabbedPane tabSpecific;
    private javax.swing.JSplitPane tabSpecificResult;
    private javax.swing.JRadioButton vegetarian;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

}
