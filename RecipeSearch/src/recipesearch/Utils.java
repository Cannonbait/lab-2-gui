/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package recipesearch;

import java.util.List;
import se.chalmers.ait.dat215.lab2.Ingredient;

/**
 *
 * @author Ivar
 */
public class Utils {
    public static String listToString(List<Ingredient> ingredientsList){
        StringBuilder sIngredients = new StringBuilder("");
        for (Ingredient ingredient : ingredientsList){
            sIngredients.append(ingredient.getAmount()).append(" ").append(ingredient.getUnit()).append(" ").append(ingredient.getName()).append("\n");
        }
        
        return sIngredients.toString();
    }
}
